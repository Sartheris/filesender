﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace FileSender {

    public partial class MainForm : Form {

        private IPAddress address = IPAddress.Parse("78.83.94.121");
        private NetworkStream stream;
        private FileInfo file;
        private FileStream fileStream;
        private TcpClient client;

        public MainForm() {
            InitializeComponent();
            textBox2.Click += textBox2_Click;
        }

        void resetControls() {
            textBox2.Enabled = button1.Enabled = true;
            button1.Text = "Send";
        }

        void textBox2_Click(object sender, EventArgs e) {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                textBox2.Text = ofd.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            textBox2.Enabled = button1.Enabled = false;
            try {
                file = new FileInfo(textBox2.Text);
                fileStream = file.OpenRead();
            } catch {
                MessageBox.Show("Error opening file");
                resetControls();
                return;
            }
            client = new TcpClient();
            try {
                client.Connect(address, 8080);
            } catch {
                MessageBox.Show("Error connecting to destination");
                resetControls();
                return;
            }
            stream = client.GetStream();
            identifySelf();
            int i = stream.ReadByte();
            if (i == 1) {
                // permission granted
            } else {
                // permission denied
                return;
            }
            byte sendType = 1;
            stream.WriteByte(sendType);
            if (sendType == 0) {
                sendRawData();
            } else if (sendType == 1) {
                sendFile();
            }
            closeConnection();
        }

        private void identifySelf() {
            string identity = "Sartheris";
            byte[] byteItentity = ASCIIEncoding.ASCII.GetBytes(identity);
            stream.Write(byteItentity, 0, byteItentity.Length);
        }

        private void sendRawData() {

        }

        private void sendFile() {
            byte[] fileName = ASCIIEncoding.ASCII.GetBytes(file.Name);
            byte[] fileNameLength = BitConverter.GetBytes(fileName.Length);
            stream.Write(fileNameLength, 0, fileNameLength.Length);
            stream.Write(fileName, 0, fileName.Length);
            int read;
            int totalWritten = 0;
            byte[] buffer = new byte[32 * 1024];
            while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0) {
                stream.Write(buffer, 0, read);
                totalWritten += read;
            }
        }

        private void closeConnection() {
            fileStream.Dispose();
            client.Close();
            //stream.Dispose();
            MessageBox.Show("Sending complete!");
            resetControls();
        }
    }
}